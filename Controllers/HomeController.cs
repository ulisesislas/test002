﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Test002.Models;

namespace Test002.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    public IActionResult Alviter()
    {
        var persona = new Persona(){
            Nombre = "Marco Antonio",
            Apellidos = "Alviter Rodriguez",
            Edad = 22,
            Estatus = true,
            Imagen = "alviter.jpg"
        };
        return View("Alviter",persona);
    }

    public IActionResult Ulises()
    {
        var persona = new Persona(){
            Nombre ="Ulises",
            Apellidos= "Islas Escudero",
            Edad = 21,
            Estatus = true,
            Imagen = "ulises.jpg",
        };
        return View("Ulises", persona);
    }

    public IActionResult Angiie(){
        var persona = new Persona(){
            Nombre = "Angiie",
            Apellidos ="Luna",
            Edad = 28,
            Estatus = true,
            Imagen = "Perro.jpg"

        };
        return View("Angiie",persona);
    }
     public IActionResult Paty(){
        var persona = new Persona(){
            Nombre = "Paty",
            Apellidos ="Acevedo",
            Edad = 28,
            Estatus = true,
            Imagen = "paty.jpg"

        };
        return View("paty",persona);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
