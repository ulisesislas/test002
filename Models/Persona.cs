namespace Test002.Models{
    public class Persona{

        public string? Nombre{get; set;}

        public string? Apellidos{get; set;}

        public int Edad{get; set;}

        public bool Estatus{get; set;}

        public string? Imagen{get; set;}
    }
}